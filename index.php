<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script type="text/javascript"src="./js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"src="./js/main.js"></script>
    <script type="text/javascript"src="./js/jquery.fademover.js"></script>
    <title>CChara Maker 3</title>
    <style type="text/css">
    div.tabbox { margin: 0px 0 0 0; padding: 0px; width:auto; height:50px;}
    p.tabs { margin: 0px; padding: 0px; }
    p.tabs a {
      display: block; width: 100px; float: left;
      margin: 0px 0px 0px 0px; padding: 0px;
      text-align: center;
      font-size:12px;
    }
    p.tabs a {text-decoration:none; background-color:#fafafa;border-right:1px solid #dcdcdc;border-left:1px solid #dcdcdc;border-top: 3px solid rgba(221, 179, 225, 0.85);  color: #666; font-weight:600;}
    /*p.tabs a:hover {opacity:0.7;filter:alpha(opacity=70);}*/
    .tab {
       height: 176px;
       width: 857px;
       float: left;
    }
    div.tab { border: 2px solid #DDD; background-color: #fbfbfb; }
    div.tab p { margin: 0.5em; }
  </style>
  <title>CChara Maker</title>
  </head>
  <body>
    <div id="wrapwrap">
      <div id="wrap_2">
        <div id="wrap">
          <div id="top">
            <div id="logoimg">
              <img src="./image/logo2.png" alt="logo" title="logo">
            </div>
            <div id="logotxt">
              <h1 id="h1">CCharaMaker Version1.12</h1>
            </div>
          </div>
          <div id="display">
            <div id="display_display">
              <canvas id="canvas" width="400" height="400" >
              </canvas>
            </div>
          </div>
          <div id="panel">
            <div class="select_page">
              <div class="picker_box">
                <div id="color_tab1">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      髪の色を選択してね
                    </p>
                  </div>
                  <form id="hair_color">
                    <input type="radio"class="button"id="hair_b_01"name="colora"value="01.png" onclick="div_radio_color('hair_color_box','hair_color_box01')"><label for="hair_b_01">        <div class="color hair_color_box hair_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_02"name="colora"value="02.png" onclick="div_radio_color('hair_color_box','hair_color_box02')"><label for="hair_b_02">        <div class="color hair_color_box hair_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_03"name="colora"value="03.png" onclick="div_radio_color('hair_color_box','hair_color_box03')"><label for="hair_b_03">        <div class="color hair_color_box hair_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_04"name="colora"value="04.png" onclick="div_radio_color('hair_color_box','hair_color_box04')"><label for="hair_b_04">        <div class="color hair_color_box hair_color_box04 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_05"name="colora"value="05.png" onclick="div_radio_color('hair_color_box','hair_color_box05')" checked><label for="hair_b_05"><div class="color hair_color_box hair_color_box05 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_06"name="colora"value="06.png" onclick="div_radio_color('hair_color_box','hair_color_box06')"><label for="hair_b_06">        <div class="color hair_color_box hair_color_box06 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_07"name="colora"value="07.png" onclick="div_radio_color('hair_color_box','hair_color_box07')"><label for="hair_b_07">        <div class="color hair_color_box hair_color_box07 button_hover"></div></label>
                    <input type="radio"class="button"id="hair_b_08"name="colora"value="08.png" onclick="div_radio_color('hair_color_box','hair_color_box08')"><label for="hair_b_08">        <div class="color hair_color_box hair_color_box08 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab2">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      肌の色を選択してね
                    </p>
                  </div>
                  <form id="head_color"action="save_cookie.php"method="save_cookie.php">
                    <input type="radio"class="button"id="head_b_01"name="colora"value="01.png"onclick="div_radio_color('head_color_box','head_color_box01')"checked><label for="head_b_01"><div class="color head_color_box head_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="head_b_02"name="colora"value="02.png"onclick="div_radio_color('head_color_box','head_color_box02')"><label for="head_b_02">       <div class="color head_color_box head_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="head_b_03"name="colora"value="03.png"onclick="div_radio_color('head_color_box','head_color_box03')"><label for="head_b_03">       <div class="color head_color_box head_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="head_b_04"name="colora"value="04.png"onclick="div_radio_color('head_color_box','head_color_box04')"><label for="head_b_04">       <div class="color head_color_box head_color_box04 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab3">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      目の色を選択してね
                    </p>
                  </div>
                  <form id="face_color">
                    <input type="radio"class="button"id="face_b_01"name="colora"value="01.png"onclick="div_radio_color('face_color_box','face_color_box01')"><label for="face_b_01">       <div class="color face_color_box face_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="face_b_02"name="colora"value="02.png"onclick="div_radio_color('face_color_box','face_color_box02')"checked><label for="face_b_02"><div class="color face_color_box face_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="face_b_03"name="colora"value="03.png"onclick="div_radio_color('face_color_box','face_color_box03')"><label for="face_b_03">       <div class="color face_color_box face_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="face_b_04"name="colora"value="04.png"onclick="div_radio_color('face_color_box','face_color_box04')"><label for="face_b_04">       <div class="color face_color_box face_color_box04 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab4">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      上着の色を選択してね
                    </p>
                  </div>
                  <form id="outer_color">
                    <input type="radio"class="button"id="outer_b_01"name="colora"value="01.png"onclick="div_radio_color('outer_color_box','outer_color_box01')"><label for="outer_b_01">       <div class="color outer_color_box outer_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="outer_b_02"name="colora"value="02.png"onclick="div_radio_color('outer_color_box','outer_color_box02')"checked><label for="outer_b_02"><div class="color outer_color_box outer_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="outer_b_03"name="colora"value="03.png"onclick="div_radio_color('outer_color_box','outer_color_box03')"><label for="outer_b_03">       <div class="color outer_color_box outer_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="outer_b_04"name="colora"value="04.png"onclick="div_radio_color('outer_color_box','outer_color_box04')"><label for="outer_b_04">       <div class="color outer_color_box outer_color_box04 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab5">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      シャツの色を選択してね
                    </p>
                  </div>
                  <form id="inner_color">
                    <input type="radio"class="button"id="inner_b_01"name="colora"value="01.png"onclick="div_radio_color('inner_color_box','inner_color_box01')"><label for="inner_b_01">       <div class="color inner_color_box inner_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_02"name="colora"value="02.png"onclick="div_radio_color('inner_color_box','inner_color_box02')"><label for="inner_b_02">       <div class="color inner_color_box inner_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_03"name="colora"value="03.png"onclick="div_radio_color('inner_color_box','inner_color_box03')"><label for="inner_b_03">       <div class="color inner_color_box inner_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_04"name="colora"value="04.png"onclick="div_radio_color('inner_color_box','inner_color_box04')"><label for="inner_b_04">       <div class="color inner_color_box inner_color_box04 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_05"name="colora"value="05.png"onclick="div_radio_color('inner_color_box','inner_color_box05')"><label for="inner_b_05">       <div class="color inner_color_box inner_color_box05 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_06"name="colora"value="06.png"onclick="div_radio_color('inner_color_box','inner_color_box06')"><label for="inner_b_06">       <div class="color inner_color_box inner_color_box06 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_07"name="colora"value="07.png"onclick="div_radio_color('inner_color_box','inner_color_box07')"checked><label for="inner_b_07"><div class="color inner_color_box inner_color_box07 button_hover"></div></label>
                    <input type="radio"class="button"id="inner_b_08"name="colora"value="08.png"onclick="div_radio_color('inner_color_box','inner_color_box08')"><label for="inner_b_08">       <div class="color inner_color_box inner_color_box08 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab6">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      ボトムの色を選択してね
                    </p>
                  </div>
                  <form id="bottom_color">
                    <input type="radio"class="button"id="bottom_b_01"name="colora"value="01.png"onclick="div_radio_color('bottom_color_box','bottom_color_box01')"><label for="bottom_b_01">       <div class="color bottom_color_box bottom_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="bottom_b_02"name="colora"value="02.png"onclick="div_radio_color('bottom_color_box','bottom_color_box02')"checked><label for="bottom_b_02"><div class="color bottom_color_box bottom_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="bottom_b_03"name="colora"value="03.png"onclick="div_radio_color('bottom_color_box','bottom_color_box03')"><label for="bottom_b_03">       <div class="color bottom_color_box bottom_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="bottom_b_04"name="colora"value="04.png"onclick="div_radio_color('bottom_color_box','bottom_color_box04')"><label for="bottom_b_04">       <div class="color bottom_color_box bottom_color_box04 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab7">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      靴の色を選択してね
                    </p>
                  </div>
                  <form id="shoes_color">
                    <input type="radio"class="button"id="shoes_b_01"name="colora"value="01.png"onclick="div_radio_color('shoes_color_box','shoes_color_box01')"checked><label for="shoes_b_01"><div class="color shoes_color_box shoes_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="shoes_b_02"name="colora"value="02.png"onclick="div_radio_color('shoes_color_box','shoes_color_box02')"><label for="shoes_b_02">       <div class="color shoes_color_box shoes_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="shoes_b_03"name="colora"value="03.png"onclick="div_radio_color('shoes_color_box','shoes_color_box03')"><label for="shoes_b_03">       <div class="color shoes_color_box shoes_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="shoes_b_04"name="colora"value="04.png"onclick="div_radio_color('shoes_color_box','shoes_color_box04')"><label for="shoes_b_04">       <div class="color shoes_color_box shoes_color_box04 button_hover"></div></label>
                  </form>
                </div>
                <div id="color_tab8">
                  <div class="colormenu">
                    <p class="colormenu_txt">
                      背景の色を選択してね
                    </p>
                  </div>
                  <form id="other_color">
                    <input type="radio"class="button"id="other_b_01"name="colora"value="01.png"onclick="div_radio_color('other_color_box','other_color_box01')"><label for="other_b_01">       <div class="color other_color_box other_color_box01 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_02"name="colora"value="02.png"onclick="div_radio_color('other_color_box','other_color_box02')"><label for="other_b_02">       <div class="color other_color_box other_color_box02 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_03"name="colora"value="03.png"onclick="div_radio_color('other_color_box','other_color_box03')"><label for="other_b_03">       <div class="color other_color_box other_color_box03 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_04"name="colora"value="04.png"onclick="div_radio_color('other_color_box','other_color_box04')"checked><label for="other_b_04"><div class="color other_color_box other_color_box04 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_05"name="colora"value="05.png"onclick="div_radio_color('other_color_box','other_color_box05')"><label for="other_b_05">       <div class="color other_color_box other_color_box05 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_06"name="colora"value="06.png"onclick="div_radio_color('other_color_box','other_color_box06')"><label for="other_b_06">       <div class="color other_color_box other_color_box06 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_07"name="colora"value="07.png"onclick="div_radio_color('other_color_box','other_color_box07')"><label for="other_b_07">       <div class="color other_color_box other_color_box07 button_hover"></div></label>
                    <input type="radio"class="button"id="other_b_08"name="colora"value="08.png"onclick="div_radio_color('other_color_box','other_color_box08')"><label for="other_b_08">       <div class="color other_color_box other_color_box08 button_hover"></div></label>
                  </form>
                </div>
              </div>
              <div class="tabbox_box">
                <div class="tabbox">
                  <p class="tabs">
                    <a href="#tab1" class="tab1" onclick="ChangeTab('tab1');ChangeColorTab('color_tab1'); return false;">髪形</a>
                    <a href="#tab2" class="tab2" onclick="ChangeTab('tab2');ChangeColorTab('color_tab2'); return false;">輪郭</a>
                    <a href="#tab3" class="tab3" onclick="ChangeTab('tab3');ChangeColorTab('color_tab3'); return false;">顔</a>
                    <a href="#tab4" class="tab4" onclick="ChangeTab('tab4');ChangeColorTab('color_tab4'); return false;">上着</a>
                    <a href="#tab5" class="tab5" onclick="ChangeTab('tab5');ChangeColorTab('color_tab5'); return false;">シャツ</a>
                    <a href="#tab6" class="tab6" onclick="ChangeTab('tab6');ChangeColorTab('color_tab6'); return false;">ボトム</a>
                    <a href="#tab7" class="tab7" onclick="ChangeTab('tab7');ChangeColorTab('color_tab7'); return false;">靴</a>
                    <a href="#tab8" class="tab8" onclick="ChangeTab('tab8');ChangeColorTab('color_tab8'); return false;">背景</a>
                  </p>
                  <div id="tabbox_scroll">
                    <div id="tab1" class="tab">
                      <form id="hair_select">
                        <input type="radio"class="button"id="hair_select_b_01"name="hair"value="./image/chara/hair_01_"onclick="div_radio('hair_1','hair_box')"checked><label for="hair_select_b_01"><div id="hair_1"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_02"name="hair"value="./image/chara/hair_02_"onclick="div_radio('hair_2','hair_box')">       <label for="hair_select_b_02"><div id="hair_2"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_03"name="hair"value="./image/chara/hair_03_"onclick="div_radio('hair_3','hair_box')">       <label for="hair_select_b_03"><div id="hair_3"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_04"name="hair"value="./image/chara/hair_04_"onclick="div_radio('hair_4','hair_box')">       <label for="hair_select_b_04"><div id="hair_4"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_05"name="hair"value="./image/chara/hair_05_"onclick="div_radio('hair_5','hair_box')">       <label for="hair_select_b_05"><div id="hair_5"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_06"name="hair"value="./image/chara/hair_06_"onclick="div_radio('hair_6','hair_box')">       <label for="hair_select_b_06"><div id="hair_6"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_07"name="hair"value="./image/chara/hair_07_"onclick="div_radio('hair_7','hair_box')">       <label for="hair_select_b_07"><div id="hair_7"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_08"name="hair"value="./image/chara/hair_08_"onclick="div_radio('hair_8','hair_box')">       <label for="hair_select_b_08"><div id="hair_8"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_09"name="hair"value="./image/chara/hair_09_"onclick="div_radio('hair_9','hair_box')">       <label for="hair_select_b_09"><div id="hair_9"class="tab_select_ico hair_box button_hover" ></div></label>
                        <input type="radio"class="button"id="hair_select_b_10"name="hair"value="./image/chara/hair_10_"onclick="div_radio('hair_10','hair_box')">       <label for="hair_select_b_10"><div id="hair_10"class="tab_select_ico hair_box button_hover"></div></label>
                        <input type="radio"class="button"id="hair_select_b_11"name="hair"value="./image/chara/hair_11_"onclick="div_radio('hair_11','hair_box')">       <label for="hair_select_b_11"><div id="hair_11"class="tab_select_ico hair_box button_hover"></div></label>
                      </form>
                    </div>
                    <div id="tab2" class="tab">
                      <form id="head_select">
                        <input id="head1"class="button"type="radio"name="head"value="./image/chara/head_01_" onclick="div_radio('head_1','head_box')"checked><label for="head1"><div id="head_1"class="tab_select_ico head_box button_hover" ></div></label>
                        <input id="head2"class="button"type="radio"name="head"value="./image/chara/head_02_" onclick="div_radio('head_2','head_box')">       <label for="head2"><div id="head_2"class="tab_select_ico head_box button_hover"></div></label>
                        <input id="head3"class="button"type="radio"name="head"value="./image/chara/head_03_" onclick="div_radio('head_3','head_box')">       <label for="head3"><div id="head_3"class="tab_select_ico head_box button_hover"></div></label>
                        <input id="head4"class="button"type="radio"name="head"value="./image/chara/head_04_" onclick="div_radio('head_4','head_box')">       <label for="head4"><div id="head_4"class="tab_select_ico head_box button_hover"></div></label>
                      </div>
                    </form>
                    <div id="tab3" class="tab">
                      <form id="face_select">
                        <input id="face1"class="button"type="radio"name="facea"value="./image/chara/face_01_" onclick="div_radio('face_1','face_box')"checked><label for="face1"><div id="face_1"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face2"class="button"type="radio"name="facea"value="./image/chara/face_02_" onclick="div_radio('face_2','face_box')">       <label for="face2"><div id="face_2"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face3"class="button"type="radio"name="facea"value="./image/chara/face_03_" onclick="div_radio('face_3','face_box')">       <label for="face3"><div id="face_3"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face4"class="button"type="radio"name="facea"value="./image/chara/face_04_" onclick="div_radio('face_4','face_box')">       <label for="face4"><div id="face_4"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face5"class="button"type="radio"name="facea"value="./image/chara/face_05_" onclick="div_radio('face_5','face_box')">       <label for="face5"><div id="face_5"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face6"class="button"type="radio"name="facea"value="./image/chara/face_06_" onclick="div_radio('face_6','face_box')">       <label for="face6"><div id="face_6"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face7"class="button"type="radio"name="facea"value="./image/chara/face_07_" onclick="div_radio('face_7','face_box')">       <label for="face7"><div id="face_7"class="tab_select_ico face_box button_hover"></div></label>
                        <input id="face8"class="button"type="radio"name="facea"value="./image/chara/face_08_" onclick="div_radio('face_8','face_box')">       <label for="face8"><div id="face_8"class="tab_select_ico face_box button_hover"></div></label>
                      </form>
                    </div>
                    <div id="tab4" class="tab">
                      <form id="outer_select">
                        <input id="outer1"class="button"type="radio"name="outer"value="./image/chara/outer_01_"onclick="div_radio('outer_1','outer_box')"checked><label for="outer1"><div id="outer_1"class="tab_select_ico outer_box button_hover" ></div></label>
                        <input id="outer2"class="button"type="radio"name="outer"value="./image/chara/outer_02_"onclick="div_radio('outer_2','outer_box')">       <label for="outer2"><div id="outer_2"class="tab_select_ico outer_box button_hover" ></div></label>
                        <input id="outer3"class="button"type="radio"name="outer"value="./image/chara/outer_03_"onclick="div_radio('outer_3','outer_box')">       <label for="outer3"><div id="outer_3"class="tab_select_ico outer_box button_hover" ></div></label>
                        <input id="outer4"class="button"type="radio"name="outer"value="./image/chara/outer_04_"onclick="div_radio('outer_4','outer_box')">       <label for="outer4"><div id="outer_4"class="tab_select_ico outer_box button_hover" ></div></label>
                      </form>
                    </div>
                    <div id="tab5" class="tab">
                      <form id="inner_select">
                        <input id="inner1"class="button"type="radio"name="inner"value="./image/chara/shirt_01_"onclick="div_radio('inner_1','inner_box')"checked><label for="inner1"><div id="inner_1"class="tab_select_ico inner_box button_hover" ></div></label>
                        <input id="inner2"class="button"type="radio"name="inner"value="./image/chara/shirt_02_"onclick="div_radio('inner_2','inner_box')">       <label for="inner2"><div id="inner_2"class="tab_select_ico inner_box button_hover" ></div></label>
                        </form>
                    </div>
                    <div id="tab6" class="tab">
                      <form id="bottom_select">
                        <input id="bottom1"class="button"type="radio"name="bottoma"value="./image/chara/bottom_01_"onclick="div_radio('bottom_1','bottom_box')"checked><label for="bottom1"><div id="bottom_1"class="tab_select_ico bottom_box button_hover" ></div></label>
                        <input id="bottom2"class="button"type="radio"name="bottoma"value="./image/chara/bottom_02_"onclick="div_radio('bottom_2','bottom_box')">       <label for="bottom2"><div id="bottom_2"class="tab_select_ico bottom_box button_hover" ></div></label>
                        <input id="bottom3"class="button"type="radio"name="bottoma"value="./image/chara/bottom_03_"onclick="div_radio('bottom_3','bottom_box')">       <label for="bottom3"><div id="bottom_3"class="tab_select_ico bottom_box button_hover" ></div></label>
                      </form>
                    </div>
                    <div id="tab7" class="tab">
                      <form id="shoes_select">
                        <input id="shoes1"class="button"type="radio"name="shoes"value="./image/chara/shoes_01_"onclick="div_radio('shoes_1','shoes_box')"checked><label for="shoes1"><div id="shoes_1"class="tab_select_ico shoes_box button_hover" ></div></label>
                        <input id="shoes2"class="button"type="radio"name="shoes"value="./image/chara/shoes_02_"onclick="div_radio('shoes_2','shoes_box')">       <label for="shoes2"><div id="shoes_2"class="tab_select_ico shoes_box button_hover" ></div></label>
                      </form>
                    </div>
                    <div id="tab8" class="tab">
                      <form id="other_select">
                        <input id="other1"class="button"type="radio"name="other"value="./image/bg/bg_01_"onclick="div_radio('other_1','other_box')"checked><label for="other1"><div id="other_1"class="tab_select_ico other_box button_hover"></div></label>
                        <input id="other2"class="button"type="radio"name="other"value="./image/bg/bg_02_"onclick="div_radio('other_2','other_box')">                  <label for="other2"><div id="other_2"class="tab_select_ico other_box button_hover" ></div></label>
                        <input id="other3"class="button"type="radio"name="other"value="./image/bg/bg_03_"onclick="div_radio('other_3','other_box')">                  <label for="other3"><div id="other_3"class="tab_select_ico other_box button_hover" ></div></label>
                      </form>
                    </div>
                  </div>
                </div>
                <script type="text/javascript">
                   ChangeTab('tab1');
                   ChangeColorTab('color_tab1');
                </script>
              </div>
              <div id="menu">
                <div id="menusita">
                  <input class="square_btn"type="button" onclick="saveCanvas('png');"value="pngで保存">
                  <input class="square_btn"type="button" onclick="saveCanvas('jpeg');"value="jpegで保存">
                  <input class="square_btn"type="button" onclick="<?php ?>"value="キャッシュに保存">
                  <a id="twitter" class="square_btn" href="http://twitter.com/share?text=CCharamakaerで遊びました。"&url="" rel="nofollow" target="_blank">Twitterで共有</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="preview">
      <div style="width:300px;height:250px;">
        <canvas id="myCanvas"></canvas>
      </div>
      <div>
      </div>
    </div>
    <?php
      ?>
  </body>
</html>
